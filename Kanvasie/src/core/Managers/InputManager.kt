package core.Managers

import org.w3c.dom.events.Event
import org.w3c.dom.events.KeyboardEvent
import java.util.*
import kotlin.browser.document
import kotlin.browser.window

/**
 * Created by Alexander on 19.12.2016.
 * This file is the part of Canvasie Framework
 */

/*
*  Input manager
* */
class InputManager{
	// list of keys, that were pressed and their state
	val keyStates = hashMapOf<String, Boolean>()

	init{
		// on key press -> key state = true
		document.addEventListener("keydown", {e : Event->
			val e = e as KeyboardEvent // recast to keyboard event
			keyStates[e.key] = true;
		})

		// on key leave -> key state = false
		document.addEventListener("keyup", {e : Event->
			val e = e as KeyboardEvent // recast to keyboard event
			keyStates[e.key] = false
		});
	}

	// on key press -> key state = state
	fun onAnalogInput(keys: String, lambda:()->Unit){
		val keyList = keys.split("+")
		var shouldFire = true;
		keyList.forEach { key: String ->
			shouldFire = shouldFire && (keyStates[key] ?:false)
		}
		if(shouldFire) lambda()
	}

	// on key press -> key state = state
	fun onActionInput(keys: String, lambda:()->Unit){
		val keyList = keys.split("+")
		var cooldown = false
		if(!cooldown){
			var shouldFire = true;
			keyList.forEach { key: String ->
				shouldFire = shouldFire && (keyStates[key] ?:false)
			}
			if(shouldFire) lambda()
			cooldown = true
			window.setTimeout({ ->
				cooldown = false;
				Unit // fixme костыль
			}, 500)
		}
	}

}