package core.Managers

import org.w3c.xhr.XMLHttpRequest


class SceneLoadManager {
	lateinit var ajax: XMLHttpRequest
	infix fun load(path: String): SceneLoadManager {
		ajax = XMLHttpRequest()
		ajax.open("GET", path)
		return this
	}

	infix fun then(callback: SceneLoadManager.(ajax: XMLHttpRequest) -> Unit): SceneLoadManager {
		ajax.onloadend = { callback(ajax)}
		ajax.send()
		return this
	}

	infix fun endWith(callback: SceneLoadManager.() -> Unit): SceneLoadManager {
		callback()
		return this
	}

}