package core.Managers

import core.Core
import core.Entities.Decoration
import core.Primitives.*

class MapParser(mapDefinition: String){
	val optionsByDefinition = HashMap<String, HashMap<String, String>>()
	val tileMap = TileMapManager()
	var decorationLayers = HashMap<String, Layer>()

	init{
		val tileLayers = mapDefinition.split("define")
		tileLayers.forEach { layer ->
			if(layer.isEmpty()) return@forEach
			val options = HashMap<String, String>()
			val nameAndData = layer.trim().split("{")
			val name = nameAndData[0]
			val lines = nameAndData[1].trim('}').split("\n");
			lines.forEach{ line ->
				if(!line.isEmpty()){
					var option = line.trim()
					if(option[0] == '#') option = ""
					if (option.contains('#')) option = line.split("#")[0]
					if (!option.isEmpty()){
						val tmp = option.split("=")
						options.put(tmp[0].trim(), tmp[1].trim(' ', '\t', '\n',';'))
					}
				}
			}
			optionsByDefinition.put(name, options)
		}
		optionsByDefinition.forEach { definition ->
			generateFrom(definition.key, definition.value)
		}
	}

	fun generateFrom (name: String, options: HashMap<String, String>){
		when(options["type"]){
			"tilemap" -> {
				if(options.contains("path")){
					val manager = SceneLoadManager()
					manager load options["path"]!! then { response ->
						tileMap.generateMap(
								response.responseText,
								name, options,
								Spritesheet(options["spritesheet"]!!)
						)
						val layers = tileMap.getLayers()
						if(!options["filter"].isNullOrEmpty())
							layers.forEach { layer ->
								layer.first.canvas.style.filter = options["filter"] ?: ""
							}
						Core.addToRunningQueue(*layers)
						Core.camera.addToQueue(*layers)
					}
				}
			}
			"decoration" -> {
				val item : Decoration
				val layer = Core.layerManager.getLayer(name, getScreenSize())
				decorationLayers.put(name, layer)
				val size= options["size"]?.split("x")
				val pos = options["pos"]?.split("x")
				item = Decoration(
						name, options["spitesheet"].orEmpty(),
						size = Dimensions(size!![0].toInt(), size[1].toInt()),
						posOnCanvas = Vector2D(pos!![0].toInt(), pos[1].toInt()),
						isStatic = options["state"].equals("static")
				)
				layer.attach(item)
				if(!options["sort-order"].isNullOrEmpty()) layer.canvas.setAttribute("sort-order", options["sort-order"]!!)
				Core.addToRunningQueue(Pair(layer, 0.3))
				Core.camera.addToQueue(Pair(layer, 0.3))
			}
		}
	}
}