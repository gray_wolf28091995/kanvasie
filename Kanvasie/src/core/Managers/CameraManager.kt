package core.Managers

import core.Primitives.*
import core.Primitives.Nodes.Node
import utils.Interfaces

class CameraManager: Node("CameraMan"), Interfaces.Controllable{

	private var sceneSize: Dimensions
	private var xSideBounds: Int
	private var ySideBoundsTop = 500
	private var ySideBoundsBottom = 500

	private var layerMap = mutableMapOf<Layer, Double>()

	init{
		this.sceneSize = getScreenSize()
		xSideBounds = (sceneSize.width/2).toInt() - 100
	}

	fun addToQueue(vararg pairs: Pair<Layer, Double>){
		pairs.forEach { item ->
			layerMap.put(item.first, item.second)
		}
	}

	fun follow(node: Node){
		node.attach(this)
	}
	fun leave(){
		if(parent == null) return
		position = parent?.position ?: Vector2D()
		parent!!.detach(this)
	}

	var difference = Vector2D();
	var posX: Int = 0
	var posY: Int = 0
	var screenTop: Int = 0
	var screenBottom: Double = 0.0
	var screenLeft: Int = 0
	var screenRight: Double = 0.0
	override fun handleInput(dt: Double) {
		if(parent == null) return
		difference.toZero()

		posX = parent!!.position.x.toInt()
		posY = parent!!.position.y.toInt()
		screenTop = ySideBoundsTop
		screenBottom = sceneSize.height- ySideBoundsBottom
		screenLeft = xSideBounds
		screenRight = sceneSize.width - xSideBounds


		if (posX < screenLeft){ // left
			difference.x -= screenLeft - posX
		}

		if (posX > screenRight){ // right
			difference.x += posX - screenRight
		}

		if (posY < screenTop){ // top
			difference.y -= screenTop - posY
		}

		if (posY > screenBottom){ // bottom
			difference.y += posY - screenBottom
		}

		difference.invLocal()
		parent!!.position.addLocal(difference.mul(0.3).toInt())
		layerMap.forEach { item ->
			item.key.position.addLocal(difference.mul(item.value).toInt())
		}
	}
}
