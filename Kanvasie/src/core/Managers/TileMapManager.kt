package core.Managers

import core.Core
import core.Physics.Physics
import core.Primitives.*
import core.Primitives.Nodes.PhysicalNode
import org.w3c.dom.CanvasRenderingContext2D
import utils.Interfaces

/**
 * Created by Alexander on 04.01.2017.
 * This file is the part of Canvasie Framework
 */
class TileMapManager(){
	var mapDescription = mutableMapOf<Char, Pair<Int, Int>>()

	var collidableTiles = emptyArray<String>()

	private val layers = mutableMapOf<Layer, Double>()

	fun generateMap(map: String, layerName: String, mapSettings: HashMap<String, String>, image: Spritesheet){
		layers.clear()
		val layer = Core.layerManager.getLayer(layerName, getScreenSize())
		if(!mapSettings["sort-order"].isNullOrEmpty())
			layer.canvas.setAttribute("sort-order", mapSettings["sort-order"]!!)

		val tmp = mapSettings["size"]?.split("x")
		TileSettings.height = tmp?.get(0)?.toInt() ?: 64
		TileSettings.width = tmp?.get(1)?.toInt() ?: 64
		TileSettings.texturePack = image
		collidableTiles = mapSettings["collidable"]
				?.split(',')
				?.map { char -> return@map char.trim() }
				?.toTypedArray()
				?: emptyArray()
		mapDescription.clear()
		mapSettings["definition"]?.split(",")?.forEach { tileDefinition ->
			val tmp = tileDefinition.split("->","x").map{ item -> return@map item.trim() }
			mapDescription.put(
				tmp[0].elementAt(0),
				Pair( tmp[1].toInt(), tmp[2].toInt())
			)
		}
		var y = 0
		map.split("\n").forEach{ line ->
			var x = 0
			line.forEach { symbol ->
				if(symbol != ' ' && symbol.toInt() != 13){
					TileSettings.posOnMap = Vector2D((TileSettings.width *x).toDouble(), TileSettings.height*y.toDouble())
					TileSettings.posOnAtlas = mapDescription[symbol] ?: Pair(0, 0)
					val tile = Tile(TileSettings)
					layer.attach(tile)
					if(collidableTiles.contains(symbol.toString()))
						Physics.addTo(tile)
				}
				x++
			}
			y++
		}
		layers.put(layer, 0.3)// implement multilayer
	}

	fun getLayers(): Array<Pair<Layer, Double>> {
		return this.layers.toList().toTypedArray()
	}
}

object TileSettings{
	lateinit var texturePack : Spritesheet
	val size		= 64
	var posOnMap	= Vector2D()
	var width 		= 64
	var height		= 64
	var posOnAtlas	= Pair(1,1)
}

class Tile(var settings: TileSettings ) : PhysicalNode("tile"), Interfaces.Drawable {
	private var size: Double

	private var  posOnAtlasX: Double
	private var posOnAtlasY: Double

	init {
		this.width  = settings.width
		this.height = settings.height
		this.size   = settings.size.toDouble()
		position	= settings.posOnMap
		this.type   = Physics.Types.STATIC

		this.posOnAtlasX = size*settings.posOnAtlas.second
		this.posOnAtlasY = size*settings.posOnAtlas.first
	}

	override fun draw(dt: Double, ctx: CanvasRenderingContext2D) {
		val pos: Vector2D
		if(this.parent != null)
			pos = position.add(this.parent!!.position);
		else
			pos = position
		settings.texturePack.draw(
				ctx,
				posOnAtlasX, posOnAtlasY,
				size, size,
				pos.x, pos.y,
				width.toDouble(), height.toDouble()
		)
	}
}