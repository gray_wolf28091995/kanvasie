package core.Managers

import core.Primitives.Dimensions
import core.Primitives.Layer
import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document
import kotlin.dom.addClass

class LayerManager{
	var layerQueue = HashMap<String, Layer>()

	/* Creates a canvas and context with the given width and height. */
	fun getLayer(id: String, dims: Dimensions): Layer {
		var canvas = document.getElementById(id) as HTMLCanvasElement?
		canvas?.addClass("isRunning")
		if(canvas === null) {
			canvas = document.createElement("canvas") as HTMLCanvasElement
			canvas.id = id
			// canvas.style.draw = "none" ? fixme зачем
			canvas.style.backgroundColor = "transparent"
			document.body?.appendChild(canvas)
		}

		val ctx = canvas.getContext("2d") as CanvasRenderingContext2D
		ctx.clearRect(0.0, 0.0, canvas.width.toDouble(), canvas.height.toDouble())

		canvas.width = dims.width.toInt()
		canvas.height = dims.height.toInt()
		val layer = Layer(id, ctx, canvas, dims)
		layerQueue[id] = layer
		return layer
	}
}