package core.Primitives

import org.w3c.dom.*
import kotlin.browser.document

/**
 * Created by Alexander on 16.12.2016.
 * This file is the part of Canvasie Framework
 */
class Spritesheet(url: String) { //fixme проверить?
	var img: Image = Image()
	var isPattern = false
	lateinit private var  pattern: Pattern //
	init{
		img.src = url
	}

	fun createPattern(dims: Dimensions? = null, pos: Vector2D? = null){
		pattern = Pattern(img, dims ?: getScreenSize(), pos ?: Vector2D())
	}

	fun draw(ctx: CanvasRenderingContext2D, spritePosX: Double, spritePosY: Double,
			 onSpriteWidth: Double, onSpriteHeight: Double, canvasPosX: Double, canvasPosY: Double,
			 canvasWidth: Double, canvasHeight: Double) {
		ctx.drawImage(img, spritePosX, spritePosY, onSpriteWidth, onSpriteHeight, canvasPosX, canvasPosY, onSpriteWidth, onSpriteHeight);
	}

	fun  draw(dt: Double, ctx: CanvasRenderingContext2D) {
		pattern.draw(dt, ctx)
	}
}