package core.Primitives

import org.w3c.dom.*
import org.w3c.dom.css.CSSStyleDeclaration
import org.w3c.dom.events.Event
import kotlin.browser.window



fun getScreenSize(): Dimensions {
	return Dimensions(960.0, 540.0)
	// return Dimensions(window.innerWidth.toDouble(), window.innerHeight.toDouble())
}

external class Image: HTMLImageElement{
	override val style: CSSStyleDeclaration = definedExternally

	override fun after(vararg nodes: dynamic) = definedExternally

	override fun before(vararg nodes: dynamic) = definedExternally

	override fun remove() = definedExternally

	override fun replaceWith(vararg nodes: dynamic) = definedExternally

	override var oncopy: ((Event) -> dynamic)? = definedExternally
	override var oncut: ((Event) -> dynamic)? = definedExternally
	override var onpaste: ((Event) -> dynamic)? = definedExternally
	override var contentEditable: String = definedExternally
	override val isContentEditable: Boolean = definedExternally

	override fun convertPointFromNode(point: DOMPointInit, from: dynamic, options: ConvertCoordinateOptions): DOMPoint = definedExternally

	override fun convertQuadFromNode(quad: dynamic, from: dynamic, options: ConvertCoordinateOptions): DOMQuad = definedExternally

	override fun convertRectFromNode(rect: DOMRectReadOnly, from: dynamic, options: ConvertCoordinateOptions): DOMQuad = definedExternally

	override fun getBoxQuads(options: BoxQuadOptions): Array<DOMQuad> = definedExternally

	override var onabort: ((Event) -> dynamic)? = definedExternally
	override var onblur: ((Event) -> dynamic)? = definedExternally
	override var oncancel: ((Event) -> dynamic)? = definedExternally
	override var oncanplay: ((Event) -> dynamic)? = definedExternally
	override var oncanplaythrough: ((Event) -> dynamic)? = definedExternally
	override var onchange: ((Event) -> dynamic)? = definedExternally
	override var onclick: ((Event) -> dynamic)? = definedExternally
	override var onclose: ((Event) -> dynamic)? = definedExternally
	override var oncontextmenu: ((Event) -> dynamic)? = definedExternally
	override var oncuechange: ((Event) -> dynamic)? = definedExternally
	override var ondblclick: ((Event) -> dynamic)? = definedExternally
	override var ondrag: ((Event) -> dynamic)? = definedExternally
	override var ondragend: ((Event) -> dynamic)? = definedExternally
	override var ondragenter: ((Event) -> dynamic)? = definedExternally
	override var ondragexit: ((Event) -> dynamic)? = definedExternally
	override var ondragleave: ((Event) -> dynamic)? = definedExternally
	override var ondragover: ((Event) -> dynamic)? = definedExternally
	override var ondragstart: ((Event) -> dynamic)? = definedExternally
	override var ondrop: ((Event) -> dynamic)? = definedExternally
	override var ondurationchange: ((Event) -> dynamic)? = definedExternally
	override var onemptied: ((Event) -> dynamic)? = definedExternally
	override var onended: ((Event) -> dynamic)? = definedExternally
	override var onerror: ((dynamic, String, Int, Int, Any?) -> dynamic)? = definedExternally
	override var onfocus: ((Event) -> dynamic)? = definedExternally
	override var oninput: ((Event) -> dynamic)? = definedExternally
	override var oninvalid: ((Event) -> dynamic)? = definedExternally
	override var onkeydown: ((Event) -> dynamic)? = definedExternally
	override var onkeypress: ((Event) -> dynamic)? = definedExternally
	override var onkeyup: ((Event) -> dynamic)? = definedExternally
	override var onload: ((Event) -> dynamic)? = definedExternally
	override var onloadeddata: ((Event) -> dynamic)? = definedExternally
	override var onloadedmetadata: ((Event) -> dynamic)? = definedExternally
	override var onloadend: ((Event) -> dynamic)? = definedExternally
	override var onloadstart: ((Event) -> dynamic)? = definedExternally
	override var onmousedown: ((Event) -> dynamic)? = definedExternally
	override var onmouseenter: ((Event) -> dynamic)? = definedExternally
	override var onmouseleave: ((Event) -> dynamic)? = definedExternally
	override var onmousemove: ((Event) -> dynamic)? = definedExternally
	override var onmouseout: ((Event) -> dynamic)? = definedExternally
	override var onmouseover: ((Event) -> dynamic)? = definedExternally
	override var onmouseup: ((Event) -> dynamic)? = definedExternally
	override var onpause: ((Event) -> dynamic)? = definedExternally
	override var onplay: ((Event) -> dynamic)? = definedExternally
	override var onplaying: ((Event) -> dynamic)? = definedExternally
	override var onprogress: ((Event) -> dynamic)? = definedExternally
	override var onratechange: ((Event) -> dynamic)? = definedExternally
	override var onreset: ((Event) -> dynamic)? = definedExternally
	override var onresize: ((Event) -> dynamic)? = definedExternally
	override var onscroll: ((Event) -> dynamic)? = definedExternally
	override var onseeked: ((Event) -> dynamic)? = definedExternally
	override var onseeking: ((Event) -> dynamic)? = definedExternally
	override var onselect: ((Event) -> dynamic)? = definedExternally
	override var onshow: ((Event) -> dynamic)? = definedExternally
	override var onstalled: ((Event) -> dynamic)? = definedExternally
	override var onsubmit: ((Event) -> dynamic)? = definedExternally
	override var onsuspend: ((Event) -> dynamic)? = definedExternally
	override var ontimeupdate: ((Event) -> dynamic)? = definedExternally
	override var ontoggle: ((Event) -> dynamic)? = definedExternally
	override var onvolumechange: ((Event) -> dynamic)? = definedExternally
	override var onwaiting: ((Event) -> dynamic)? = definedExternally
	override var onwheel: ((Event) -> dynamic)? = definedExternally
	override val nextElementSibling: Element? = definedExternally
	override val previousElementSibling: Element? = definedExternally
	override val children: HTMLCollection = definedExternally
	override val firstElementChild: Element? = definedExternally
	override val lastElementChild: Element? = definedExternally

	override fun append(vararg nodes: dynamic) = definedExternally

	override fun prepend(vararg nodes: dynamic) = definedExternally

	override fun querySelector(selectors: String): Element? = definedExternally

	override fun querySelectorAll(selectors: String): NodeList = definedExternally

	override val assignedSlot: HTMLSlotElement? = definedExternally
	override val childElementCount: Int

}

external class Date(){
	fun getTime(): Int = definedExternally
	fun now(): Int = definedExternally
}

//fun switch(conditions:Boolean, (Boolean) -> Unit){
//	conditions.forEach { condition ->
//		condition.invoke()
//	}
//}
