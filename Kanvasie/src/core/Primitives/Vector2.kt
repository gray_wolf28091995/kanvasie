package core.Primitives

abstract class Vector2<T>(var x: T, var y: T){
	/** Subtracts the given vector from this vector.**/
	abstract fun sub(vector: Vector2<T>): Vector2<T>

	/** Adds the given vector to this vector.**/
	abstract fun add(vector: Vector2<T>): Vector2<T>

	/** Multiplies the given vector with this vector.**/
	abstract fun mul(n: Double): Vector2<T>

	/** Returns the inverse of this vector.**/
	abstract fun inv() : Vector2<T>

	/** Calculates the squared length of this vector. **/
	abstract fun length() : Double

	/** Product **/
	abstract fun dot(vector : Vector2<T>): Double

	/** Copy method **/
	abstract fun copy() : Vector2<T>

	/** Calculates the squared distance between this vector and the given vector.**/
	abstract fun dist(vector: Vector2<T>): Double

	/** Calculates the normalized form of this vector. **/
	abstract fun normalize(): Vector2<T>

	abstract fun interpolate(vector: Vector2<T>, step: Double): Vector2<T>

	abstract fun interpolateLocal(vector: Vector2<T>, step: Double)

	/** Determines if this vector is within the bounds defined by the given vectors.**/
	abstract fun inBound(topLeft: Vector2<T>, bottomRight: Vector2<T>): Boolean

	/** **/
	abstract fun toZero()

	/** Returns a string representing this vector. **/
	abstract override fun toString(): String

	abstract fun addLocal(vector: Vector2<Double>): Vector2<Double>
	abstract fun invLocal(): Vector2D
	abstract fun mulLocal(n: Double): Vector2D
	abstract fun toInt(): Vector2D
}