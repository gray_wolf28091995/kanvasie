package core.Primitives

import org.w3c.dom.CanvasPattern
import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.CaretPosition
import utils.Interfaces

/**
 * Created by Alexander on 24.12.2016.
 * This file is the part of Canvasie Framework
 */

class Pattern(val img: Image, val sizes: Dimensions, var position: Vector2D = Vector2D()): Interfaces.Drawable {
	private var pat: CanvasPattern? = null;
	override fun draw(dt: Double, ctx: CanvasRenderingContext2D) {
		if(pat == null)
			pat = ctx.createPattern(img, "repeat"); // FIXME cache result with resource manager
		ctx.rect(position.x, position.y, sizes.width, sizes.height);
		ctx.fillStyle = pat;
		ctx.fill()
	}
}