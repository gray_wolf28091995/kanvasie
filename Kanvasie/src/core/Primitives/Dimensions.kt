package core.Primitives


data class Dimensions(val width: Double, val height: Double){
	constructor(width: Int, height: Int) : this(width.toDouble(), height.toDouble())
}
