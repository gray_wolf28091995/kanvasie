package core.Primitives

import kotlin.js.Math


class Vector2D(x: Double = 0.0, y:Double = 0.0): Vector2<Double>(x, y){

	constructor(x: Int, y: Int): this(x.toDouble(), y.toDouble())

	override fun add(vector: Vector2<Double>) = Vector2D(x + vector.x, y + vector.y)
	override fun sub(vector: Vector2<Double>) = Vector2D(x - vector.x, y - vector.y)
	override fun dot(vector: Vector2<Double>) = vector.x * x + vector.y * y
	override fun mul(n: Double)= Vector2D(x * n, y * n)
	override fun inv() = mul(-1.0)
	override fun length() =(x*x + y*y).toDouble()
	override fun copy() = Vector2D(x, y)
	override fun interpolate(vector: Vector2<Double>, step: Double): Vector2<Double> {
		return mul(step).addLocal(vector.mul(1.0 - step))
	}
	override fun dist(vector: Vector2<Double>): Double {
		val dx = x - vector.x
		val dy = y - vector.y
		return dx*dx + dy*dy
	}
	override fun normalize(): Vector2<Double> {
		val length = Math.sqrt(length())
		return Vector2D(x/length, y/length)
	}

	override fun addLocal(vector: Vector2<Double>): Vector2<Double> {
		x += vector.x
		y += vector.y
		return this
	}
	override fun mulLocal(n: Double): Vector2D {
		x *= n
		y *= n
		return this
	}
	override fun invLocal() = mulLocal(-1.0)
	override fun interpolateLocal(vector: Vector2<Double>, step: Double) {
		mulLocal(step).addLocal(vector.mul(1.0 - step))
	}

	override fun inBound(topLeft: Vector2<Double>, bottomRight: Vector2<Double>): Boolean {
		return (topLeft.x < x && x < bottomRight.x
				&& topLeft.y < y && y < bottomRight.y)
	}

	override fun toString() = "Vector2D{$x, $y}"
	override fun toZero(){ x = 0.0; y = 0.0 }
	override fun toInt(): Vector2D {
		x = Math.ceil(x).toDouble()
		y = Math.ceil(y).toDouble()
		return this
	}
}