package core.Primitives

import core.Primitives.Nodes.Node
import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import utils.Interfaces



class Layer(
		name: String,
		val ctx: CanvasRenderingContext2D,
		val canvas: HTMLCanvasElement,
		override var dims: Dimensions = Dimensions(0.0, 0.0)
): Node(name), Interfaces.Destructable{

	private var canBeUpdated = true
	var isStatic: Boolean = false
	var dimsV2D: Vector2D
			= Vector2D(dims.width-64, dims.height-64)

	init {
		console.log(dims)
	}
	/* Отрисовка слоя*/
	fun updateLayer(dt: Double){
		if(canBeUpdated) { // Отрисовываем фон один раз fixme проверять через CameraMan или какое-то событие
			ctx.clearRect(0.0, 0.0, dims.width, dims.height)
			this.traverse { node: Node ->
				if (node is Interfaces.Controllable) node.handleInput(dt)
				if(!node.isVisible()) return@traverse
				if (node is Interfaces.Drawable) node.draw(dt, ctx)
			}
			if(this.isStatic) canBeUpdated = false // Если это статический слой, тогда обновляем его один раз
		}
	}

	override fun destruct() {
		canvas.remove()
	}
}

