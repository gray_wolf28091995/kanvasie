package core.Primitives.Nodes

import core.Primitives.Dimensions
import core.Primitives.Vector2D
import core.Primitives.getScreenSize


abstract class Node(var name: String = "", vararg childNodes: Node) {
	// Местоположение узла
	var position = Vector2D(0.0, 0.0)
	// Угол разворота узла
	var rotation = 0F
	// Размеры узла
	var scale = 1F

	var parent: Node? = null
	var children = emptyArray<Node>()

	open var dims: Dimensions = getScreenSize()

	init {
		this.attach(*childNodes)
	}

	// Присоединение узла
	open fun attach(vararg nodes: Node) {
		nodes.forEach { node ->
			node.parent = this
			children = children.plus(node)
		}
	}

	fun detach(node: Node) {
		children.drop(children.indexOf(node))
	}
	// Обход дерева
	fun traverse(callback: (Node) -> Unit) {
		callback(this)
		children.forEach { node: Node -> node.traverse(callback) }
	}

	var i = 0
	fun isVisible(): Boolean {
		i++
		if(parent == null) return false
		var bottomRight = parent!!.position.copy()
		bottomRight.invLocal()
		bottomRight.addLocal(Vector2D(dims.width + 64, dims.height + 64))
		if(i%700 == 0) console.log(parent!!.position, bottomRight);
		if(position.inBound(parent!!.position, bottomRight))
			return true
		return false
	}

	override fun toString(): String {
		var s = name
		if (!children.isEmpty()) {
			s += " => " + children.map { "\n\t "+ it.toString()}
		}else{
			s += " => <empty>\n"
		}
		return s
	}
}