package core.Primitives.Nodes

import core.Physics.Collision
import core.Physics.Physics
import core.Primitives.Vector2D

/**
 * Created by Alexander on 24.12.2016.
 * This file is the part of Canvasie Framework
 */

abstract class PhysicalNode(name: String, vararg  childNodes: Node) : Node(name, *childNodes){

	// Physical values
	private var canBeRotated: Boolean = false
	private var restitution: Double = 0.0
	private var density: Double = 0.0
	private var friction: Double = 0.0
	private var velocity: Vector2D = Vector2D(0.0, 0.0)
			var acceleration: Vector2D = Vector2D(0.0, 0.0)

	// Размеры
	private var halfWidth: Double
	private var halfHeight: Double
	var width: Int = 0
	var height: Int = 0
	var collision: Int = Physics.Types.DISPLACE
	var type: Int = Physics.Types.STATIC

	init {
		halfWidth = width * .5
		halfHeight = height * .5
		when(collision){
//			Physics.Types.ELASTIC  -> { Collision.displace() }
//			Physics.Types.DISPLACE -> { Collision.elastic(restitution) }
		}

		// Velocity
		// Acceleration

		// Update the bounds of the object to recalculate
		// the half sizes and any other pieces
		this.updateBounds()
	}

	// Update bounds includes the rect's
	// boundary updates
	fun updateBounds() {
		halfWidth = width * .5
		halfHeight = height * .5
	}

	// Getters for the mid point of the rect
	fun getMidX() = halfWidth + parent!!.position.x + position.x

	fun getMidY() = halfHeight + parent!!.position.y + position.y

	// Getters for the top, left, right, and bottom
	// of the rectangle
	fun getTop()	= parent!!.position.y + position.y
	fun getLeft()	= parent!!.position.x + position.x
	fun getRight()	= parent!!.position.x + position.x + width
	fun getBottom()	= parent!!.position.y + position.y + height

	fun  checkCollisions(dt: Double, nodes: MutableList<PhysicalNode>) { // проверяем коллизии по отношению к доступным узлам
        nodes.forEach { node ->
			if(this === node) return@forEach
			if(node is PhysicalNode && this !== node){
				// Если есть хотябы одна коллизия
				if(Collision.detectRect(this, node)){
					Collision.resolve(this, node)
				}
			}
		}
	}
}