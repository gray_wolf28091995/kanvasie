package core.Entities

import core.Primitives.Dimensions
import core.Primitives.Nodes.Node
import core.Primitives.Spritesheet
import core.Primitives.Vector2D
import org.w3c.dom.CanvasRenderingContext2D
import utils.Interfaces

/**
 * Created by Alexander on 23.12.2016.
 * This file is the part of Canvasie Framework
 */
open class Decoration(name: String, url: String,
					  var size: Dimensions = Dimensions(0.0, 0.0),
					  var posOnAtlas: Vector2D = Vector2D(),
					  var posOnCanvas: Vector2D = Vector2D(),
					  var isStatic: Boolean = false
): Node(name), Interfaces.Drawable {

	var sprite: Spritesheet
	init{
		sprite = Spritesheet(url)
		if(size == null) size = Dimensions(sprite.img.clientHeight, sprite.img.clientWidth)
	}

	override fun draw(dt: Double, ctx: CanvasRenderingContext2D) {
		if(isStatic) this.position = posOnCanvas.copy();
		sprite.draw(ctx,
				posOnAtlas.x, posOnAtlas.y,
				size.width, size.height,
				this.position.x, this.position.y,
				size.width, size.height
		)
	}
}