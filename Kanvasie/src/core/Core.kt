package core

import core.Managers.CameraManager
import core.Managers.InputManager
import core.Managers.LayerManager
import core.Physics.Physics
import core.Primitives.Layer
import org.w3c.dom.Element
import org.w3c.dom.asList
import org.w3c.dom.get
import utils.Interfaces
import kotlin.browser.document
import kotlin.browser.window
import kotlin.dom.asElementList
import kotlin.dom.childElements
import kotlin.dom.clear
import kotlin.dom.get

/**
* Объект, отвечающий за запустк движка. Запуск просиходит через метод run.
* */
object Core{
	// todo CameraMan
	// Настройки движка
	private var  state: String = "running" /* paused | stopped | dev_mode*/
	private var runtimeZone = document.getElementById("runtime")

	private var onRunningQueue = mutableMapOf<Layer, Double>()

	private var onPauseQueue = mutableListOf<Layer>()


	// Менеджеры ресурсов
	val layerManager	: LayerManager	= LayerManager()				// Менеджер слоёв канваса
	val inputManager	: InputManager	= InputManager()				// Менеджер ввода
	var camera			: CameraManager	= CameraManager()				// Менеджер камеры



	private var lastTime = 0.0
	private var dt = 0.0
	/**
	*  Метод запуска приложения
	**/
	fun run(passedTime: Double = 0.0){
		dt = (passedTime - lastTime)/10
		if(state == "running")
			Physics.updateCollision(dt)
		Core.render(dt)
		window.requestAnimationFrame{passedTime -> Core.run(passedTime)}
		lastTime = passedTime
	}

	/**  Метод снятия с паузы приложения **/
	fun start() {state = "running"}

	/** Метод приостановки приложения **/
	fun stop() {state = "paused"}

	fun addToRunningQueue(vararg pairs: Pair<Layer, Double>){
		pairs.forEach { layerAndRatio ->
			runtimeZone?.append(layerAndRatio.first.canvas)
			onRunningQueue.put(layerAndRatio.first, layerAndRatio.second)
		}
		val list = runtimeZone?.querySelectorAll("canvas")?.asList()?.toTypedArray()
		list?.sort { first, second ->
			val x = (first as Element).getAttribute("sort-order")?.toInt() ?: 0
			val y = (second as Element).getAttribute("sort-order")?.toInt() ?: 0
			if(x > y) return@sort 1
			if(x == y) return@sort  0
			else return@sort  -1
		}
		runtimeZone?.append(*list!!)
//		runtimeZone?.clear()
	}

	fun addToPauseQueue(layer: Layer){
		onPauseQueue.add(layer)
	}

	fun switchScene(){ // on scene change - delete all layers
		onRunningQueue.forEach{
			if(it is Interfaces.Destructable)
				it.destruct()
		}
		onRunningQueue = mutableMapOf<Layer, Double>()
	}

	fun render(dt: Double){
		if (state == "running")// on running queud
			onRunningQueue.forEach{ it.key.updateLayer(dt) }
		else // on pause queu
			onPauseQueue.forEach{ it.updateLayer(dt) }
	}
}