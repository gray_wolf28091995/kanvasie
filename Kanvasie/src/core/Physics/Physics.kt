package core.Physics

import core.Primitives.Nodes.PhysicalNode
import core.Primitives.Vector2D
import core.Primitives.getScreenSize

object Physics{
	val gravity = Vector2D(0.0, 9.0)
	private val space = mutableListOf<PhysicalNode>()
	object Types{
		// Тип колизии
		val DISPLACE  = -1
		val ELASTIC   = -2

		// Тип физического объекта
		val STATIC =  0
		val DYNAMIC   =  1
	}

	fun updateCollision(dt:Double) {
		space.forEach { item ->
			if(item.type !== Physics.Types.STATIC) { // если узел статический, тогда идём дальше
				if(!item.isVisible()) return@forEach
				item.acceleration.interpolateLocal(Physics.gravity, 0.7);
				item.checkCollisions(dt, space)
				item.position.addLocal(item.acceleration)
				item.acceleration.toZero()
			}
		}
	}

	fun addTo(vararg nodes: PhysicalNode){
		space += nodes
	}
}