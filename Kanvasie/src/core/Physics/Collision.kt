package core.Physics

import core.Primitives.Nodes.PhysicalNode
import core.Primitives.Vector2D

/**
 * Created by Alexander on 25.12.2016.
 * This file is the part of Canvasie Framework
 */
object Collision{

	private var  restitution: Double = 0.0

	fun elastic(restitution: Double?) {
		this.restitution = restitution ?: 0.2
	}

	fun displace(){ }


	private var x1: Double = 0.0
	private var y1: Double = 0.0
	private var x2: Double = 0.0
	private var y2: Double = 0.0
	private var _x1: Double = 0.0
	private var _y1: Double = 0.0
	private var _x2: Double = 0.0
	private var _y2: Double = 0.0
	/**
	 *			|											  |
	 *		   \|/ y1										 \|/ _y1
	 *		  top											 top
	 *	  *******************						   *******************
	 * ---> *				 * --->				 ---> *				 * --->
	 * left *				 * right				left *				 * right
	 *  x1  *				 *  x2				  _x1  *				 *  _x2
	 *	  *******************						   *******************
	 *		   bottom | y2								   bottom | _y2
	 *				 \|/										   \|/
	 * */
	fun detectRect(collider: PhysicalNode, collidee: PhysicalNode): Boolean {
		// Store the collider and collidee edges
		x1 = collider.getLeft();
		y1 = collider.getTop();
		x2 = collider.getRight();
		y2 = collider.getBottom();

		_x1 = collidee.getLeft();
		_y1 = collidee.getTop();
		_x2 = collidee.getRight();
		_y2 = collidee.getBottom();

		// If the any of the edges are beyond any of the
		// others, then we know that the box cannot be
		// colliding
		if ( x2 < _x1 || x1 > _x2 || y2 < _y1 || y1 > _y2) {
			return false;
		}
		// If the algorithm made it here, it had to collide
		return true;
	}

	private var colliderMidX: Double = 0.0
	private var colliderMidY: Double = 0.0
	private var collideeMidX: Double = 0.0
	private var collideeMidY: Double = 0.0
	private var colliderAcceleration: Vector2D = Vector2D()
	private var fromTop: Boolean = false
	private var fromLeft: Boolean = false
	fun resolve(collider: PhysicalNode, collidee: PhysicalNode){
		// Find the mid points of the collidee and collider
		colliderMidX = collider.getMidX();
		colliderMidY = collider.getMidY();
		collideeMidX = collidee.getMidX();
		collideeMidY = collidee.getMidY();

		colliderAcceleration = collider.acceleration

		fromTop = colliderMidY < collideeMidY
		fromLeft = colliderMidX < collideeMidX

		when{// fixme сжать
			fromTop  && fromLeft  -> { // сверху и слева
				// если слева и выше крышки
				if (collider.getBottom() > collidee.getTop()){ // пригаем вверх
					var difference = collider.getBottom() - collidee.getTop();
					if(colliderAcceleration.y > difference)
						colliderAcceleration.y -= difference;
				}else{ // fixme исправить колизию со сторон (когда 2 блока рядом срабатывает коллизия)
//					if(colliderMidY > collidee.getTop() && collider.getRight() > collidee.getLeft()){ //прыгаем влево
//						colliderAcceleration.x -= collider.getRight() - collidee.getLeft();
//					}
				}
			}
			fromTop  && !fromLeft ->{ // сверху справа
				// если слева и выше крышки
				if (collider.getBottom() > collidee.getTop()){ // пригаем вверх
					colliderAcceleration.y -= collider.getBottom() - collidee.getTop();
				}else{
//					if(colliderMidY > collidee.getTop() && collider.getLeft() > collidee.getRight()){ //прыгаем вправо
//						colliderAcceleration.x += collidee.getRight() - collider.getLeft();
//					}
				}
			}
			!fromTop && fromLeft  ->{ // снизу слева
				// если слева и выше крышки
				if (collider.getTop() < collidee.getBottom()){ // пригаем вниз
					colliderAcceleration.y += collidee.getBottom() - collider.getTop();
				}else {
					if(collider.getLeft() > collidee.getRight()){ //прыгаем влево
						colliderAcceleration.x -= collider.getRight() - collidee.getLeft();
					}
				}
			}
			!fromTop && !fromLeft ->{ // снизу справа
				// если слева и выше крышки fixme дичь
				if (collider.getTop() > collidee.getBottom()){ // пригаем вниз
					colliderAcceleration.y -= collider.getTop() - collidee.getBottom();
				}else {
					if(collider.getRight() > collidee.getLeft()){ //прыгаем вправо
						colliderAcceleration.x += collidee.getRight() - collider.getLeft();
					}
				}
			}
		}
	}
}
