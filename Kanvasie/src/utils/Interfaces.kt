package utils

import org.w3c.dom.CanvasRenderingContext2D

/**
 * Created by Alexander on 20.12.2016.
 * This file is the part of Canvasie Framework
 */

sealed class Interfaces(){
	/** Интерфейс, для разрушаемых объектов**/
	interface Destructable {
		fun destruct()
	}

	interface Controllable {
		fun handleInput(dt: Double)
	}

	interface Drawable {
		fun draw(dt: Double, ctx: CanvasRenderingContext2D)
	}
}