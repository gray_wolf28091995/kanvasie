package platformer.Entities

import core.Entities.Decoration
import core.Primitives.getScreenSize
import org.w3c.dom.CanvasRenderingContext2D

/**
 * Created by Alexander on 23.12.2016.
 * This file is the part of Kanvasie Framework
 */
class Background(url: String, val isPattern: Boolean = false): Decoration("background", url) {
	private var sizes = getScreenSize()
	init {
		if (isPattern) sprite.createPattern()
	}
	override fun draw(dt: Double, ctx: CanvasRenderingContext2D) {
//		// Create gradient
//		val grd = ctx.createLinearGradient(110.000, 0.000, 110.000, sizes.height)
//
//		// Add colors
//		grd.addColorStop(0.557, "rgba(46, 130, 215, 1.000)")
//		grd.addColorStop(0.845, "rgba(215, 130, 130, 1.000)")
//		grd.addColorStop(0.954, "rgba(215, 196, 179, 1.000)")
//
//		// Fill with gradient
//		ctx.fillStyle = grd
//		ctx.fillRect(0.0, 0.0, sizes.width, sizes.height)
		sprite.draw(ctx, 0.0, 0.0, sprite.img.width.toDouble(), sprite.img.height.toDouble(), 0.0, 0.0, sizes.width, sizes.height)
	}
}