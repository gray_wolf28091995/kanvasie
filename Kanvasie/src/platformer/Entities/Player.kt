package platformer.Entities

import core.Core
import core.Physics.Physics
import core.Primitives.*
import core.Primitives.Nodes.Node
import core.Primitives.Nodes.PhysicalNode
import org.w3c.dom.CanvasRenderingContext2D
import utils.Interfaces
import kotlin.js.Math

/**www
 * Created by Alexander on 16.12.2016.
 * This file is the part of Canvasie Framework
 */
class Player(name: String, vararg childNodes: Node) : PhysicalNode(name, *childNodes), Interfaces.Drawable, Interfaces.Controllable{

	var speed = .6 // <1
	private var sprite: Spritesheet = Spritesheet("assets/coin.png")
	private val numberOfFrames = 10.0 // fixme animate class
	private val steps = Math.abs(1/speed).toInt() // 2
	init {
		var screenSize = getScreenSize()
		width = 50
		height = 40
		this.type = Physics.Types.DYNAMIC
		position = Vector2D(screenSize.width/2, screenSize.height/2)
	}

	override fun handleInput(dt: Double) {
		Core.inputManager.onAnalogInput("a", {acceleration.x -=2*dt})
//		Core.inputManager.onAnalogInput("w", {acceleration.y -=0*dt})
		Core.inputManager.onAnalogInput("d", {acceleration.x +=2*dt})
//		Core.inputManager.onAnalogInput("s", {acceleration.y +=0*dt})
		Core.inputManager.onAnalogInput(" ", {acceleration.y -=4*dt})
	}

	private var leftTopCorner = 0.0;
	private var currentStep = 0;
	override fun draw(dt: Double, ctx: CanvasRenderingContext2D) {
		sprite.draw(ctx, 44.0*(leftTopCorner), 0.0, 44.0, 40.0, position.x, position.y, 44.0, 40.0);
		if(currentStep == steps){
			leftTopCorner++
			currentStep = 0
		}else currentStep++

		if(leftTopCorner == numberOfFrames) leftTopCorner = 0.0
	}
}