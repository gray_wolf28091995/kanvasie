package platformer.scene

import core.Core
import core.Managers.MapParser
import core.Managers.SceneLoadManager
import core.Physics.Physics
import core.Primitives.*
import platformer.Entities.Background
import platformer.Entities.Player
import kotlin.dom.clear
import kotlin.dom.get

class Scene_GamingClub{
	init {
	}

	init{
		val backgroundLayer = Core.layerManager.getLayer("Background", getScreenSize())
		val mainLayer = Core.layerManager.getLayer("Kanvasie", getScreenSize())
		backgroundLayer.attach(Background("assets/sky.png", false))
		backgroundLayer.isStatic = true
		backgroundLayer.canvas.setAttribute("sort-order", "-100")
		mainLayer.canvas.setAttribute("sort-order", "0")

		val player = Player("Player")
		mainLayer.attach(player)
		Physics.addTo(player)
		Core.camera.follow(player)
		Core.addToRunningQueue(Pair(backgroundLayer, 0.001))
		Core.addToRunningQueue(Pair(mainLayer, 0.001))
		console.log(mainLayer.toString())
		var mapLoader = SceneLoadManager()
		mapLoader load "assets/map/map.def" then { response ->
			var t = MapParser(response.responseText)
		}
	}
}