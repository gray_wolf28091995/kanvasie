package platformer.scene

import core.Core
import core.Primitives.Vector2D
import core.Primitives.getScreenSize
import platformer.Entities.Player

/**
 * Created by Alexander on 20.12.2016.
 * This file is the part of Canvasie Framework
 */
class Scene_TempleOfCoin{
	init {
		var mainLayer = Core.layerManager.getLayer("Kanvasie", getScreenSize());
		mainLayer.attach(Player("Player"))
		var player_2 = Player("2")
		player_2.position.add(Vector2D(10.0, 10.0))
		mainLayer.attach(player_2)
//		Core.addToRunn\	ingQueue(Pair(mainLayer, 1.0))
	}
}