package platformer

import core.Core
import platformer.scene.Scene_GamingClub
import platformer.scene.Scene_TempleOfCoin
import kotlin.browser.document
import core.Managers.TileMapManager
import org.w3c.dom.get

/**
 * Created by Alexander on 16.12.2016.
 * This file is the part of Canvasie Framework
 */
class Game{
	init{
		val core = Core
//		Core.inputManager.onActionInput("1", {
			Core.switchScene() // clearing scene
			Scene_GamingClub()
			console.log("scene 1");
			var title = document.getElementsByTagName("title")[0]
			title?.textContent = "Gaming club"
//		})

		Core.inputManager.onActionInput("2", {
			Core.switchScene()
			Scene_TempleOfCoin()
			console.log("scene 2");
			var title = document.getElementsByTagName("title")[0]
			title?.textContent = "Temple Of Coin"
		})
		core.run()
	}
}