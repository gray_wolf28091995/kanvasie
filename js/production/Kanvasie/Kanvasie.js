if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'Kanvasie'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'Kanvasie'.");
}
var Kanvasie = function (_, Kotlin) {
  'use strict';
  var asList = Kotlin.org.w3c.dom.asList_kt9thq$;
  var toInt = Kotlin.kotlin.text.toInt_pdl1vz$;
  var split = Kotlin.kotlin.text.split_ip8yn$;
  var kotlin_0 = Kotlin.kotlin;
  var addClass = Kotlin.kotlin.dom.addClass_hhb33f$;
  var HashMap_init = Kotlin.kotlin.collections.HashMap_init_q3lmfv$;
  var equals = Kotlin.kotlin.text.equals_igcy3c$;
  var Pair = Kotlin.kotlin.Pair;
  var trim_0 = Kotlin.kotlin.text.trim_wqw3xr$;
  var contains_0 = Kotlin.kotlin.text.contains_sgbm27$;
  var split_0 = Kotlin.kotlin.text.split_o64adg$;
  var contains_1 = Kotlin.kotlin.collections.contains_mjy6jw$;
  var toList = Kotlin.kotlin.collections.toList_abgq59$;
  var indexOf = Kotlin.kotlin.collections.indexOf_mjy6jw$;
  var drop = Kotlin.kotlin.collections.drop_8ujjk8$;
  Decoration.prototype = Object.create(Node.prototype);
  Decoration.prototype.constructor = Decoration;
  CameraManager.prototype = Object.create(Node.prototype);
  CameraManager.prototype.constructor = CameraManager;
  PhysicalNode.prototype = Object.create(Node.prototype);
  PhysicalNode.prototype.constructor = PhysicalNode;
  Tile.prototype = Object.create(PhysicalNode.prototype);
  Tile.prototype.constructor = Tile;
  Layer.prototype = Object.create(Node.prototype);
  Layer.prototype.constructor = Layer;
  Vector2D.prototype = Object.create(Vector2.prototype);
  Vector2D.prototype.constructor = Vector2D;
  Background.prototype = Object.create(Decoration.prototype);
  Background.prototype.constructor = Background;
  Player.prototype = Object.create(PhysicalNode.prototype);
  Player.prototype.constructor = Player;
  function main(args) {
    new Game();
  }
  function Core() {
    Core_instance = this;
    this.state_0 = 'running';
    this.runtimeZone_0 = document.getElementById('runtime');
    this.onRunningQueue_0 = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$();
    this.onPauseQueue_0 = Kotlin.kotlin.collections.ArrayList_init_ww73n8$();
    this.layerManager = new LayerManager();
    this.inputManager = new InputManager();
    this.camera = new CameraManager();
    this.lastTime_0 = 0.0;
    this.dt_0 = 0.0;
  }
  function Core$run$lambda(passedTime) {
    Core_getInstance().run_14dthe$(passedTime);
  }
  Core.prototype.run_14dthe$ = function (passedTime) {
    if (passedTime === void 0)
      passedTime = 0.0;
    this.dt_0 = (passedTime - this.lastTime_0) / 10;
    if (Kotlin.equals(this.state_0, 'running'))
      Physics_getInstance().updateCollision_14dthe$(this.dt_0);
    Core_getInstance().render_14dthe$(this.dt_0);
    window.requestAnimationFrame(Core$run$lambda);
    this.lastTime_0 = passedTime;
  };
  Core.prototype.start = function () {
    this.state_0 = 'running';
  };
  Core.prototype.stop = function () {
    this.state_0 = 'paused';
  };
  function Core$addToRunningQueue$lambda(first, second) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4;
    var x = (tmp$_1 = (tmp$_0 = (Kotlin.isType(tmp$ = first, Element) ? tmp$ : Kotlin.throwCCE()).getAttribute('sort-order')) != null ? toInt(tmp$_0) : null) != null ? tmp$_1 : 0;
    var y = (tmp$_4 = (tmp$_3 = (Kotlin.isType(tmp$_2 = second, Element) ? tmp$_2 : Kotlin.throwCCE()).getAttribute('sort-order')) != null ? toInt(tmp$_3) : null) != null ? tmp$_4 : 0;
    if (x > y)
      return 1;
    if (x === y)
      return 0;
    else
      return -1;
  }
  Core.prototype.addToRunningQueue_8agu7i$ = function (pairs) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    var tmp$_3;
    for (tmp$_3 = 0; tmp$_3 !== pairs.length; ++tmp$_3) {
      var element = pairs[tmp$_3];
      var tmp$_4;
      (tmp$_4 = this.runtimeZone_0) != null ? tmp$_4.append(element.first.canvas) : null;
      this.onRunningQueue_0.put_xwzc9p$(element.first, element.second);
    }
    var list = (tmp$_1 = (tmp$_0 = (tmp$ = this.runtimeZone_0) != null ? tmp$.querySelectorAll('canvas') : null) != null ? asList(tmp$_0) : null) != null ? Kotlin.kotlin.collections.copyToArray(tmp$_1) : null;
    if (list != null) {
      list.sort(Core$addToRunningQueue$lambda);
    }
    this.runtimeZone_0 != null ? (tmp$_2 = this.runtimeZone_0).append.apply(tmp$_2, list != null ? list : Kotlin.throwNPE()) : null;
  };
  Core.prototype.addToPauseQueue_99lmwy$ = function (layer) {
    this.onPauseQueue_0.add_11rb$(layer);
  };
  Core.prototype.switchScene = function () {
    var tmp$;
    tmp$ = this.onRunningQueue_0.entries.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      if (Kotlin.isType(element, Interfaces$Destructable))
        element.destruct();
    }
    this.onRunningQueue_0 = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$();
  };
  Core.prototype.render_14dthe$ = function (dt) {
    if (Kotlin.equals(this.state_0, 'running')) {
      var tmp$;
      tmp$ = this.onRunningQueue_0.entries.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        element.key.updateLayer_14dthe$(dt);
      }
    }
     else {
      var tmp$_0;
      tmp$_0 = this.onPauseQueue_0.iterator();
      while (tmp$_0.hasNext()) {
        var element_0 = tmp$_0.next();
        element_0.updateLayer_14dthe$(dt);
      }
    }
  };
  Core.$metadata$ = {
    kind: Kotlin.Kind.OBJECT,
    simpleName: 'Core',
    interfaces: []
  };
  var Core_instance = null;
  function Core_getInstance() {
    if (Core_instance === null) {
      new Core();
    }
    return Core_instance;
  }
  function Game() {
    var core = Core_getInstance();
    Core_getInstance().switchScene();
    new Scene_GamingClub();
    console.log('scene 1');
    var title = document.getElementsByTagName('title')[0];
    title != null ? (title.textContent = 'Gaming club') : null;
    Core_getInstance().inputManager.onActionInput_a4mwiz$('2', Game_init$lambda);
    core.run_14dthe$();
  }
  function Game_init$lambda() {
    Core_getInstance().switchScene();
    new Scene_TempleOfCoin();
    console.log('scene 2');
    var title = document.getElementsByTagName('title')[0];
    title != null ? (title.textContent = 'Temple Of Coin') : null;
  }
  Game.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Game',
    interfaces: []
  };
  function Interfaces() {
  }
  function Interfaces$Destructable() {
  }
  Interfaces$Destructable.$metadata$ = {
    kind: Kotlin.Kind.INTERFACE,
    simpleName: 'Destructable',
    interfaces: []
  };
  function Interfaces$Controllable() {
  }
  Interfaces$Controllable.$metadata$ = {
    kind: Kotlin.Kind.INTERFACE,
    simpleName: 'Controllable',
    interfaces: []
  };
  function Interfaces$Drawable() {
  }
  Interfaces$Drawable.$metadata$ = {
    kind: Kotlin.Kind.INTERFACE,
    simpleName: 'Drawable',
    interfaces: []
  };
  Interfaces.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Interfaces',
    interfaces: []
  };
  function Decoration(name, url, size, posOnAtlas, posOnCanvas, isStatic) {
    if (size === void 0)
      size = new Dimensions(0.0, 0.0);
    if (posOnAtlas === void 0)
      posOnAtlas = new Vector2D();
    if (posOnCanvas === void 0)
      posOnCanvas = new Vector2D();
    if (isStatic === void 0)
      isStatic = false;
    Node.call(this, name, []);
    this.size = size;
    this.posOnAtlas = posOnAtlas;
    this.posOnCanvas = posOnCanvas;
    this.isStatic = isStatic;
    this.sprite = null;
    this.sprite = new Spritesheet(url);
    if (this.size == null)
      this.size = Dimensions_init(this.sprite.img.clientHeight, this.sprite.img.clientWidth);
  }
  Decoration.prototype.draw_cdh3rs$ = function (dt, ctx) {
    if (this.isStatic)
      this.position = this.posOnCanvas.copy();
    this.sprite.draw_c0ii6e$(ctx, this.posOnAtlas.x, this.posOnAtlas.y, this.size.width, this.size.height, this.position.x, this.position.y, this.size.width, this.size.height);
  };
  Decoration.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Decoration',
    interfaces: [Interfaces$Drawable, Node]
  };
  function LightSource() {
  }
  LightSource.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'LightSource',
    interfaces: []
  };
  function CameraManager() {
    Node.call(this, 'CameraMan', []);
    this.sceneSize_0 = null;
    this.xSideBounds_0 = 0;
    this.ySideBoundsTop_0 = 500;
    this.ySideBoundsBottom_0 = 500;
    this.layerMap_0 = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$();
    this.sceneSize_0 = getScreenSize();
    this.xSideBounds_0 = (this.sceneSize_0.width / 2 | 0) - 100 | 0;
    this.difference = new Vector2D();
    this.posX = 0;
    this.posY = 0;
    this.screenTop = 0;
    this.screenBottom = 0.0;
    this.screenLeft = 0;
    this.screenRight = 0.0;
  }
  CameraManager.prototype.addToQueue_8agu7i$ = function (pairs) {
    var tmp$;
    for (tmp$ = 0; tmp$ !== pairs.length; ++tmp$) {
      var element = pairs[tmp$];
      this.layerMap_0.put_xwzc9p$(element.first, element.second);
    }
  };
  CameraManager.prototype.follow_ottzaq$ = function (node) {
    node.attach_dkxqd5$([this]);
  };
  CameraManager.prototype.leave = function () {
    var tmp$, tmp$_0, tmp$_1;
    if (this.parent == null)
      return;
    this.position = (tmp$_0 = (tmp$ = this.parent) != null ? tmp$.position : null) != null ? tmp$_0 : new Vector2D();
    ((tmp$_1 = this.parent) != null ? tmp$_1 : Kotlin.throwNPE()).detach_ottzaq$(this);
  };
  CameraManager.prototype.handleInput_14dthe$ = function (dt) {
    var tmp$, tmp$_0, tmp$_1;
    if (this.parent == null)
      return;
    this.difference.toZero();
    this.posX = ((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position.x | 0;
    this.posY = ((tmp$_0 = this.parent) != null ? tmp$_0 : Kotlin.throwNPE()).position.y | 0;
    this.screenTop = this.ySideBoundsTop_0;
    this.screenBottom = this.sceneSize_0.height - this.ySideBoundsBottom_0;
    this.screenLeft = this.xSideBounds_0;
    this.screenRight = this.sceneSize_0.width - this.xSideBounds_0;
    if (this.posX < this.screenLeft) {
      this.difference.x = this.difference.x - (this.screenLeft - this.posX | 0);
    }
    if (this.posX > this.screenRight) {
      this.difference.x = this.difference.x + (this.posX - this.screenRight);
    }
    if (this.posY < this.screenTop) {
      this.difference.y = this.difference.y - (this.screenTop - this.posY | 0);
    }
    if (this.posY > this.screenBottom) {
      this.difference.y = this.difference.y + (this.posY - this.screenBottom);
    }
    this.difference.invLocal();
    ((tmp$_1 = this.parent) != null ? tmp$_1 : Kotlin.throwNPE()).position.addLocal_aaofr4$(this.difference.mul_14dthe$(0.3).toInt());
    var tmp$_2;
    tmp$_2 = this.layerMap_0.entries.iterator();
    while (tmp$_2.hasNext()) {
      var element = tmp$_2.next();
      element.key.position.addLocal_aaofr4$(this.difference.mul_14dthe$(element.value).toInt());
    }
  };
  CameraManager.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'CameraManager',
    interfaces: [Interfaces$Controllable, Node]
  };
  function InputManager() {
    this.keyStates = Kotlin.kotlin.collections.HashMap_init_q3lmfv$();
    document.addEventListener('keydown', InputManager_init$lambda(this));
    document.addEventListener('keyup', InputManager_init$lambda_0(this));
  }
  InputManager.prototype.onAnalogInput_a4mwiz$ = function (keys, lambda) {
    var keyList = split(keys, ['+']);
    var shouldFire = {v: true};
    var tmp$;
    tmp$ = keyList.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      var tmp$_0;
      shouldFire.v = (shouldFire.v && ((tmp$_0 = this.keyStates.get_11rb$(element)) != null ? tmp$_0 : false));
    }
    if (shouldFire.v)
      lambda();
  };
  function InputManager$onActionInput$lambda(closure$cooldown) {
    return function () {
      closure$cooldown.v = false;
      kotlin_0.Unit;
    };
  }
  InputManager.prototype.onActionInput_a4mwiz$ = function (keys, lambda) {
    var keyList = split(keys, ['+']);
    var cooldown = {v: false};
    if (!cooldown.v) {
      var shouldFire = {v: true};
      var tmp$;
      tmp$ = keyList.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        var tmp$_0;
        shouldFire.v = (shouldFire.v && ((tmp$_0 = this.keyStates.get_11rb$(element)) != null ? tmp$_0 : false));
      }
      if (shouldFire.v)
        lambda();
      cooldown.v = true;
      window.setTimeout(InputManager$onActionInput$lambda(cooldown), 500);
    }
  };
  function InputManager_init$lambda(this$InputManager) {
    return function (e) {
      var tmp$;
      var e_0 = Kotlin.isType(tmp$ = e, KeyboardEvent) ? tmp$ : Kotlin.throwCCE();
      var $receiver = this$InputManager.keyStates;
      var key = e_0.key;
      $receiver.put_xwzc9p$(key, true);
    };
  }
  function InputManager_init$lambda_0(this$InputManager) {
    return function (e) {
      var tmp$;
      var e_0 = Kotlin.isType(tmp$ = e, KeyboardEvent) ? tmp$ : Kotlin.throwCCE();
      var $receiver = this$InputManager.keyStates;
      var key = e_0.key;
      $receiver.put_xwzc9p$(key, false);
    };
  }
  InputManager.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'InputManager',
    interfaces: []
  };
  function LayerManager() {
    this.layerQueue = HashMap_init();
  }
  LayerManager.prototype.getLayer_qbaqzy$ = function (id, dims) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    var canvas = (tmp$ = document.getElementById(id)) == null || Kotlin.isType(tmp$, HTMLCanvasElement) ? tmp$ : Kotlin.throwCCE();
    canvas != null ? addClass(canvas, ['isRunning']) : null;
    if (canvas === null) {
      canvas = Kotlin.isType(tmp$_0 = document.createElement('canvas'), HTMLCanvasElement) ? tmp$_0 : Kotlin.throwCCE();
      canvas.id = id;
      canvas.style.backgroundColor = 'transparent';
      (tmp$_1 = document.body) != null ? tmp$_1.appendChild(canvas) : null;
    }
    var ctx = Kotlin.isType(tmp$_2 = canvas.getContext('2d'), CanvasRenderingContext2D) ? tmp$_2 : Kotlin.throwCCE();
    ctx.clearRect(0.0, 0.0, canvas.width, canvas.height);
    canvas.width = dims.width | 0;
    canvas.height = dims.height | 0;
    var layer = new Layer(id, ctx, canvas, dims);
    this.layerQueue.put_xwzc9p$(id, layer);
    return layer;
  };
  LayerManager.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'LayerManager',
    interfaces: []
  };
  function MapParser(mapDefinition) {
    this.optionsByDefinition = HashMap_init();
    this.tileMap = new TileMapManager();
    this.decorationLayers = HashMap_init();
    var tileLayers = split(mapDefinition, ['define']);
    var tmp$;
    tmp$ = tileLayers.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      action$break: {
        if (element.length === 0)
          break action$break;
        var options = HashMap_init();
        var tmp$_0;
        var nameAndData = split(Kotlin.kotlin.text.trim_gw00vp$(Kotlin.isCharSequence(tmp$_0 = element) ? tmp$_0 : Kotlin.throwCCE()).toString(), ['{']);
        var name = nameAndData.get_za3lpa$(0);
        var lines = split(trim_0(nameAndData.get_za3lpa$(1), [125]), ['\n']);
        var tmp$_1;
        tmp$_1 = lines.iterator();
        while (tmp$_1.hasNext()) {
          var element_0 = tmp$_1.next();
          if (!(element_0.length === 0)) {
            var tmp$_2;
            var option = Kotlin.kotlin.text.trim_gw00vp$(Kotlin.isCharSequence(tmp$_2 = element_0) ? tmp$_2 : Kotlin.throwCCE()).toString();
            if (Kotlin.unboxChar(option.charCodeAt(0)) === 35)
              option = '';
            if (contains_0(option, 35))
              option = split(element_0, ['#']).get_za3lpa$(0);
            if (!(option.length === 0)) {
              var tmp = split(option, ['=']);
              var $receiver = tmp.get_za3lpa$(0);
              var tmp$_3;
              options.put_xwzc9p$(Kotlin.kotlin.text.trim_gw00vp$(Kotlin.isCharSequence(tmp$_3 = $receiver) ? tmp$_3 : Kotlin.throwCCE()).toString(), trim_0(tmp.get_za3lpa$(1), [32, 9, 10, 59]));
            }
          }
        }
        this.optionsByDefinition.put_xwzc9p$(name, options);
      }
    }
    var tmp$_4;
    tmp$_4 = this.optionsByDefinition.entries.iterator();
    while (tmp$_4.hasNext()) {
      var element_1 = tmp$_4.next();
      this.generateFrom_yemong$(element_1.key, element_1.value);
    }
  }
  function MapParser$generateFrom$lambda(this$MapParser, closure$name, closure$options) {
    return function ($receiver, response) {
      var tmp$_0, tmp$, tmp$_1, tmp$_2;
      tmp$_2 = this$MapParser.tileMap;
      tmp$_0 = response.responseText;
      tmp$_1 = new Spritesheet((tmp$ = closure$options.get_11rb$('spritesheet')) != null ? tmp$ : Kotlin.throwNPE());
      tmp$_2.generateMap_buv9j5$(tmp$_0, closure$name, closure$options, tmp$_1);
      var layers = this$MapParser.tileMap.getLayers();
      var $receiver_0 = closure$options.get_11rb$('filter');
      if (!($receiver_0 == null || $receiver_0.length === 0)) {
        var tmp$_3;
        for (tmp$_3 = 0; tmp$_3 !== layers.length; ++tmp$_3) {
          var element = layers[tmp$_3];
          var closure$options_0 = closure$options;
          var tmp$_4;
          element.first.canvas.style.filter = (tmp$_4 = closure$options_0.get_11rb$('filter')) != null ? tmp$_4 : '';
        }
      }
      Core_getInstance().addToRunningQueue_8agu7i$(layers.slice());
      Core_getInstance().camera.addToQueue_8agu7i$(layers.slice());
    };
  }
  MapParser.prototype.generateFrom_yemong$ = function (name, options) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    tmp$ = options.get_11rb$('type');
    if (Kotlin.equals(tmp$, 'tilemap')) {
      var tmp$_4;
      if ((Kotlin.isType(tmp$_4 = options, Kotlin.kotlin.collections.Map) ? tmp$_4 : Kotlin.throwCCE()).containsKey_11rb$('path')) {
        var manager = new SceneLoadManager();
        manager.load_61zpoe$((tmp$_0 = options.get_11rb$('path')) != null ? tmp$_0 : Kotlin.throwNPE()).then_ugvufg$(MapParser$generateFrom$lambda(this, name, options));
      }
    }
     else if (Kotlin.equals(tmp$, 'decoration')) {
      var item;
      var layer = Core_getInstance().layerManager.getLayer_qbaqzy$(name, getScreenSize());
      this.decorationLayers.put_xwzc9p$(name, layer);
      var size = (tmp$_1 = options.get_11rb$('size')) != null ? split(tmp$_1, ['x']) : null;
      var pos = (tmp$_2 = options.get_11rb$('pos')) != null ? split(tmp$_2, ['x']) : null;
      var $receiver = options.get_11rb$('spitesheet');
      item = new Decoration(name, $receiver != null ? $receiver : '', Dimensions_init(toInt((size != null ? size : Kotlin.throwNPE()).get_za3lpa$(0)), toInt(size.get_za3lpa$(1))), void 0, Vector2D_init(toInt((pos != null ? pos : Kotlin.throwNPE()).get_za3lpa$(0)), toInt(pos.get_za3lpa$(1))), equals(options.get_11rb$('state'), 'static'));
      layer.attach_dkxqd5$([item]);
      var $receiver_0 = options.get_11rb$('sort-order');
      if (!($receiver_0 == null || $receiver_0.length === 0)) {
        layer.canvas.setAttribute('sort-order', (tmp$_3 = options.get_11rb$('sort-order')) != null ? tmp$_3 : Kotlin.throwNPE());
      }
      Core_getInstance().addToRunningQueue_8agu7i$([new Pair(layer, 0.3)]);
      Core_getInstance().camera.addToQueue_8agu7i$([new Pair(layer, 0.3)]);
    }
  };
  MapParser.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'MapParser',
    interfaces: []
  };
  function SceneLoadManager() {
    this.ajax = void 0;
  }
  SceneLoadManager.prototype.load_61zpoe$ = function (path) {
    this.ajax = new XMLHttpRequest();
    this.ajax.open('GET', path);
    return this;
  };
  function SceneLoadManager$then$lambda(closure$callback, this$SceneLoadManager) {
    return function (it) {
      closure$callback(this$SceneLoadManager, this$SceneLoadManager.ajax);
    };
  }
  SceneLoadManager.prototype.then_ugvufg$ = function (callback) {
    this.ajax.onloadend = SceneLoadManager$then$lambda(callback, this);
    this.ajax.send();
    return this;
  };
  SceneLoadManager.prototype.endWith_ujtrkz$ = function (callback) {
    callback(this);
    return this;
  };
  SceneLoadManager.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'SceneLoadManager',
    interfaces: []
  };
  function TileMapManager() {
    this.mapDescription = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$();
    this.collidableTiles = [];
    this.layers_0 = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$();
  }
  TileMapManager.prototype.generateMap_buv9j5$ = function (map_1, layerName, mapSettings, image) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8, tmp$_9, tmp$_10;
    this.layers_0.clear();
    var layer = Core_getInstance().layerManager.getLayer_qbaqzy$(layerName, getScreenSize());
    var $receiver = mapSettings.get_11rb$('sort-order');
    if (!($receiver == null || $receiver.length === 0)) {
      layer.canvas.setAttribute('sort-order', (tmp$ = mapSettings.get_11rb$('sort-order')) != null ? tmp$ : Kotlin.throwNPE());
    }
    var tmp = (tmp$_0 = mapSettings.get_11rb$('size')) != null ? split(tmp$_0, ['x']) : null;
    TileSettings_getInstance().height = (tmp$_2 = (tmp$_1 = tmp != null ? tmp.get_za3lpa$(0) : null) != null ? toInt(tmp$_1) : null) != null ? tmp$_2 : 64;
    TileSettings_getInstance().width = (tmp$_4 = (tmp$_3 = tmp != null ? tmp.get_za3lpa$(1) : null) != null ? toInt(tmp$_3) : null) != null ? tmp$_4 : 64;
    TileSettings_getInstance().texturePack = image;
    var tmp$_11;
    if ((tmp$_6 = (tmp$_5 = mapSettings.get_11rb$('collidable')) != null ? split_0(tmp$_5, [44]) : null) != null) {
      var destination = Kotlin.kotlin.collections.ArrayList_init_ww73n8$(Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$(tmp$_6, 10));
      var tmp$_12;
      tmp$_12 = tmp$_6.iterator();
      while (tmp$_12.hasNext()) {
        var item = tmp$_12.next();
        var tmp$_13;
        destination.add_11rb$(Kotlin.kotlin.text.trim_gw00vp$(Kotlin.isCharSequence(tmp$_13 = item) ? tmp$_13 : Kotlin.throwCCE()).toString());
      }
      tmp$_11 = destination;
    }
     else
      tmp$_11 = null;
    this.collidableTiles = (tmp$_8 = (tmp$_7 = tmp$_11) != null ? Kotlin.kotlin.collections.copyToArray(tmp$_7) : null) != null ? tmp$_8 : [];
    this.mapDescription.clear();
    if ((tmp$_10 = (tmp$_9 = mapSettings.get_11rb$('definition')) != null ? split(tmp$_9, [',']) : null) != null) {
      var tmp$_14;
      tmp$_14 = tmp$_10.iterator();
      while (tmp$_14.hasNext()) {
        var element = tmp$_14.next();
        var $receiver_0 = split(element, ['->', 'x']);
        var destination_0 = Kotlin.kotlin.collections.ArrayList_init_ww73n8$(Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$($receiver_0, 10));
        var tmp$_15;
        tmp$_15 = $receiver_0.iterator();
        while (tmp$_15.hasNext()) {
          var item_0 = tmp$_15.next();
          var tmp$_16;
          destination_0.add_11rb$(Kotlin.kotlin.text.trim_gw00vp$(Kotlin.isCharSequence(tmp$_16 = item_0) ? tmp$_16 : Kotlin.throwCCE()).toString());
        }
        var tmp_0 = destination_0;
        var tmp$_17 = this.mapDescription;
        var $receiver_1 = tmp_0.get_za3lpa$(0);
        tmp$_17.put_xwzc9p$(Kotlin.toBoxedChar(Kotlin.unboxChar($receiver_1.charCodeAt(0))), new Pair(toInt(tmp_0.get_za3lpa$(1)), toInt(tmp_0.get_za3lpa$(2))));
      }
    }
    var y = {v: 0};
    var tmp$_18;
    tmp$_18 = split(map_1, ['\n']).iterator();
    while (tmp$_18.hasNext()) {
      var element_0 = tmp$_18.next();
      var x = {v: 0};
      var tmp$_19;
      tmp$_19 = Kotlin.kotlin.text.iterator_gw00vp$(element_0);
      while (tmp$_19.hasNext()) {
        var element_1 = tmp$_19.next();
        var symbol = Kotlin.toBoxedChar(element_1);
        var tmp$_20;
        if (Kotlin.unboxChar(symbol) !== 32 && (Kotlin.unboxChar(symbol) | 0) !== 13) {
          TileSettings_getInstance().posOnMap = new Vector2D(Kotlin.imul(TileSettings_getInstance().width, x.v), TileSettings_getInstance().height * y.v);
          TileSettings_getInstance().posOnAtlas = (tmp$_20 = this.mapDescription.get_11rb$(Kotlin.toBoxedChar(symbol))) != null ? tmp$_20 : new Pair(0, 0);
          var tile = new Tile(TileSettings_getInstance());
          layer.attach_dkxqd5$([tile]);
          if (contains_1(this.collidableTiles, String.fromCharCode(Kotlin.toBoxedChar(symbol))))
            Physics_getInstance().addTo_7issw$([tile]);
        }
        x.v = x.v + 1 | 0;
      }
      y.v = y.v + 1 | 0;
    }
    this.layers_0.put_xwzc9p$(layer, 0.3);
  };
  TileMapManager.prototype.getLayers = function () {
    var $receiver = toList(this.layers_0);
    return Kotlin.kotlin.collections.copyToArray($receiver);
  };
  TileMapManager.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'TileMapManager',
    interfaces: []
  };
  function TileSettings() {
    TileSettings_instance = this;
    this.texturePack = void 0;
    this.size = 64;
    this.posOnMap = new Vector2D();
    this.width = 64;
    this.height = 64;
    this.posOnAtlas = new Pair(1, 1);
  }
  TileSettings.$metadata$ = {
    kind: Kotlin.Kind.OBJECT,
    simpleName: 'TileSettings',
    interfaces: []
  };
  var TileSettings_instance = null;
  function TileSettings_getInstance() {
    if (TileSettings_instance === null) {
      new TileSettings();
    }
    return TileSettings_instance;
  }
  function Tile(settings) {
    PhysicalNode.call(this, 'tile', []);
    this.settings = settings;
    this.size_0 = 0;
    this.posOnAtlasX_0 = 0;
    this.posOnAtlasY_0 = 0;
    this.width = this.settings.width;
    this.height = this.settings.height;
    this.size_0 = this.settings.size;
    this.position = this.settings.posOnMap;
    this.type = Physics$Types_getInstance().STATIC;
    this.posOnAtlasX_0 = this.size_0 * this.settings.posOnAtlas.second;
    this.posOnAtlasY_0 = this.size_0 * this.settings.posOnAtlas.first;
  }
  Tile.prototype.draw_cdh3rs$ = function (dt, ctx) {
    var tmp$;
    var pos;
    if (this.parent != null) {
      pos = this.position.add_ljbjnb$(((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position);
    }
     else
      pos = this.position;
    this.settings.texturePack.draw_c0ii6e$(ctx, this.posOnAtlasX_0, this.posOnAtlasY_0, this.size_0, this.size_0, pos.x, pos.y, this.width, this.height);
  };
  Tile.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Tile',
    interfaces: [Interfaces$Drawable, PhysicalNode]
  };
  function Collision() {
    Collision_instance = this;
    this.restitution_0 = 0.0;
    this.x1_0 = 0.0;
    this.y1_0 = 0.0;
    this.x2_0 = 0.0;
    this.y2_0 = 0.0;
    this._x1_0 = 0.0;
    this._y1_0 = 0.0;
    this._x2_0 = 0.0;
    this._y2_0 = 0.0;
    this.colliderMidX_0 = 0.0;
    this.colliderMidY_0 = 0.0;
    this.collideeMidX_0 = 0.0;
    this.collideeMidY_0 = 0.0;
    this.colliderAcceleration_0 = new Vector2D();
    this.fromTop_0 = false;
    this.fromLeft_0 = false;
  }
  Collision.prototype.elastic_yrwdxb$ = function (restitution) {
    this.restitution_0 = restitution != null ? restitution : 0.2;
  };
  Collision.prototype.displace = function () {
  };
  Collision.prototype.detectRect_2nusms$ = function (collider, collidee) {
    this.x1_0 = collider.getLeft();
    this.y1_0 = collider.getTop();
    this.x2_0 = collider.getRight();
    this.y2_0 = collider.getBottom();
    this._x1_0 = collidee.getLeft();
    this._y1_0 = collidee.getTop();
    this._x2_0 = collidee.getRight();
    this._y2_0 = collidee.getBottom();
    if (this.x2_0 < this._x1_0 || this.x1_0 > this._x2_0 || this.y2_0 < this._y1_0 || this.y1_0 > this._y2_0) {
      return false;
    }
    return true;
  };
  Collision.prototype.resolve_2nusms$ = function (collider, collidee) {
    this.colliderMidX_0 = collider.getMidX();
    this.colliderMidY_0 = collider.getMidY();
    this.collideeMidX_0 = collidee.getMidX();
    this.collideeMidY_0 = collidee.getMidY();
    this.colliderAcceleration_0 = collider.acceleration;
    this.fromTop_0 = this.colliderMidY_0 < this.collideeMidY_0;
    this.fromLeft_0 = this.colliderMidX_0 < this.collideeMidX_0;
    if (this.fromTop_0 && this.fromLeft_0) {
      if (collider.getBottom() > collidee.getTop()) {
        var difference = collider.getBottom() - collidee.getTop();
        if (this.colliderAcceleration_0.y > difference)
          this.colliderAcceleration_0.y = this.colliderAcceleration_0.y - difference;
      }
    }
     else if (this.fromTop_0 && !this.fromLeft_0) {
      if (collider.getBottom() > collidee.getTop()) {
        this.colliderAcceleration_0.y = this.colliderAcceleration_0.y - (collider.getBottom() - collidee.getTop());
      }
    }
     else if (!this.fromTop_0 && this.fromLeft_0)
      if (collider.getTop() < collidee.getBottom()) {
        this.colliderAcceleration_0.y = this.colliderAcceleration_0.y + (collidee.getBottom() - collider.getTop());
      }
       else {
        if (collider.getLeft() > collidee.getRight()) {
          this.colliderAcceleration_0.x = this.colliderAcceleration_0.x - (collider.getRight() - collidee.getLeft());
        }
      }
     else if (!this.fromTop_0 && !this.fromLeft_0)
      if (collider.getTop() > collidee.getBottom()) {
        this.colliderAcceleration_0.y = this.colliderAcceleration_0.y - (collider.getTop() - collidee.getBottom());
      }
       else {
        if (collider.getRight() > collidee.getLeft()) {
          this.colliderAcceleration_0.x = this.colliderAcceleration_0.x + (collidee.getRight() - collider.getLeft());
        }
      }
  };
  Collision.$metadata$ = {
    kind: Kotlin.Kind.OBJECT,
    simpleName: 'Collision',
    interfaces: []
  };
  var Collision_instance = null;
  function Collision_getInstance() {
    if (Collision_instance === null) {
      new Collision();
    }
    return Collision_instance;
  }
  function Physics() {
    Physics_instance = this;
    this.gravity = new Vector2D(0.0, 9.0);
    this.space_0 = Kotlin.kotlin.collections.ArrayList_init_ww73n8$();
  }
  function Physics$Types() {
    Physics$Types_instance = this;
    this.DISPLACE = -1;
    this.ELASTIC = -2;
    this.STATIC = 0;
    this.DYNAMIC = 1;
  }
  Physics$Types.$metadata$ = {
    kind: Kotlin.Kind.OBJECT,
    simpleName: 'Types',
    interfaces: []
  };
  var Physics$Types_instance = null;
  function Physics$Types_getInstance() {
    if (Physics$Types_instance === null) {
      new Physics$Types();
    }
    return Physics$Types_instance;
  }
  Physics.prototype.updateCollision_14dthe$ = function (dt) {
    var tmp$;
    tmp$ = this.space_0.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      action$break: {
        if (element.type !== Physics$Types_getInstance().STATIC) {
          if (!element.isVisible())
            break action$break;
          element.acceleration.interpolateLocal_ybbuwn$(Physics_getInstance().gravity, 0.7);
          element.checkCollisions_1o88it$(dt, this.space_0);
          element.position.addLocal_aaofr4$(element.acceleration);
          element.acceleration.toZero();
        }
      }
    }
  };
  Physics.prototype.addTo_7issw$ = function (nodes) {
    var $receiver = this.space_0;
    Kotlin.kotlin.collections.addAll_ye1y7v$($receiver, nodes);
  };
  Physics.$metadata$ = {
    kind: Kotlin.Kind.OBJECT,
    simpleName: 'Physics',
    interfaces: []
  };
  var Physics_instance = null;
  function Physics_getInstance() {
    if (Physics_instance === null) {
      new Physics();
    }
    return Physics_instance;
  }
  function Dimensions(width, height) {
    this.width = width;
    this.height = height;
  }
  Dimensions.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Dimensions',
    interfaces: []
  };
  function Dimensions_init(width, height, $this) {
    $this = $this || Object.create(Dimensions.prototype);
    Dimensions.call($this, width, height);
    return $this;
  }
  Dimensions.prototype.component1 = function () {
    return this.width;
  };
  Dimensions.prototype.component2 = function () {
    return this.height;
  };
  Dimensions.prototype.copy_lu1900$ = function (width, height) {
    return new Dimensions_init(width === void 0 ? this.width : width, height === void 0 ? this.height : height);
  };
  Dimensions.prototype.toString = function () {
    return 'Dimensions(width=' + Kotlin.toString(this.width) + (', height=' + Kotlin.toString(this.height)) + ')';
  };
  Dimensions.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.width) | 0;
    result = result * 31 + Kotlin.hashCode(this.height) | 0;
    return result;
  };
  Dimensions.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.width, other.width) && Kotlin.equals(this.height, other.height)))));
  };
  function Layer(name, ctx, canvas, dims) {
    if (dims === void 0)
      dims = new Dimensions(0.0, 0.0);
    Node.call(this, name, []);
    this.ctx = ctx;
    this.canvas = canvas;
    this.dims_356if8$_0 = dims;
    this.canBeUpdated_0 = true;
    this.isStatic = false;
    this.dimsV2D = new Vector2D(this.dims.width - 64, this.dims.height - 64);
    console.log(this.dims);
  }
  Object.defineProperty(Layer.prototype, 'dims', {
    get: function () {
      return this.dims_356if8$_0;
    },
    set: function (dims) {
      this.dims_356if8$_0 = dims;
    }
  });
  function Layer$updateLayer$lambda(closure$dt, this$Layer) {
    return function (node) {
      if (Kotlin.isType(node, Interfaces$Controllable))
        node.handleInput_14dthe$(closure$dt);
      if (!node.isVisible())
        return;
      if (Kotlin.isType(node, Interfaces$Drawable))
        node.draw_cdh3rs$(closure$dt, this$Layer.ctx);
    };
  }
  Layer.prototype.updateLayer_14dthe$ = function (dt) {
    if (this.canBeUpdated_0) {
      this.ctx.clearRect(0.0, 0.0, this.dims.width, this.dims.height);
      this.traverse_s7vmat$(Layer$updateLayer$lambda(dt, this));
      if (this.isStatic)
        this.canBeUpdated_0 = false;
    }
  };
  Layer.prototype.destruct = function () {
    this.canvas.remove();
  };
  Layer.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Layer',
    interfaces: [Interfaces$Destructable, Node]
  };
  function Pattern(img, sizes, position) {
    if (position === void 0)
      position = new Vector2D();
    this.img = img;
    this.sizes = sizes;
    this.position = position;
    this.pat_0 = null;
  }
  Pattern.prototype.draw_cdh3rs$ = function (dt, ctx) {
    if (this.pat_0 == null)
      this.pat_0 = ctx.createPattern(this.img, 'repeat');
    ctx.rect(this.position.x, this.position.y, this.sizes.width, this.sizes.height);
    ctx.fillStyle = this.pat_0;
    ctx.fill();
  };
  Pattern.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Pattern',
    interfaces: [Interfaces$Drawable]
  };
  function Spritesheet(url) {
    this.img = new Image();
    this.isPattern = false;
    this.pattern_0 = void 0;
    this.img.src = url;
  }
  Spritesheet.prototype.createPattern_rfwhpy$ = function (dims, pos) {
    if (dims === void 0)
      dims = null;
    if (pos === void 0)
      pos = null;
    this.pattern_0 = new Pattern(this.img, dims != null ? dims : getScreenSize(), pos != null ? pos : new Vector2D());
  };
  Spritesheet.prototype.draw_c0ii6e$ = function (ctx, spritePosX, spritePosY, onSpriteWidth, onSpriteHeight, canvasPosX, canvasPosY, canvasWidth, canvasHeight) {
    ctx.drawImage(this.img, spritePosX, spritePosY, onSpriteWidth, onSpriteHeight, canvasPosX, canvasPosY, onSpriteWidth, onSpriteHeight);
  };
  Spritesheet.prototype.draw_cdh3rs$ = function (dt, ctx) {
    this.pattern_0.draw_cdh3rs$(dt, ctx);
  };
  Spritesheet.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Spritesheet',
    interfaces: []
  };
  function getScreenSize() {
    return new Dimensions(960.0, 540.0);
  }
  function Vector2(x, y) {
    this.x = x;
    this.y = y;
  }
  Vector2.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Vector2',
    interfaces: []
  };
  function Vector2D(x, y) {
    if (x === void 0)
      x = 0.0;
    if (y === void 0)
      y = 0.0;
    Vector2.call(this, x, y);
  }
  Vector2D.prototype.add_ljbjnb$ = function (vector) {
    return new Vector2D(this.x + vector.x, this.y + vector.y);
  };
  Vector2D.prototype.sub_ljbjnb$ = function (vector) {
    return new Vector2D(this.x - vector.x, this.y - vector.y);
  };
  Vector2D.prototype.dot_ljbjnb$ = function (vector) {
    return vector.x * this.x + vector.y * this.y;
  };
  Vector2D.prototype.mul_14dthe$ = function (n) {
    return new Vector2D(this.x * n, this.y * n);
  };
  Vector2D.prototype.inv = function () {
    return this.mul_14dthe$(-1.0);
  };
  Vector2D.prototype.length = function () {
    return this.x * this.x + this.y * this.y;
  };
  Vector2D.prototype.copy = function () {
    return new Vector2D(this.x, this.y);
  };
  Vector2D.prototype.interpolate_ybbuwn$ = function (vector, step) {
    return this.mul_14dthe$(step).addLocal_aaofr4$(vector.mul_14dthe$(1.0 - step));
  };
  Vector2D.prototype.dist_ljbjnb$ = function (vector) {
    var dx = this.x - vector.x;
    var dy = this.y - vector.y;
    return dx * dx + dy * dy;
  };
  Vector2D.prototype.normalize = function () {
    var length = Math.sqrt(this.length());
    return new Vector2D(this.x / length, this.y / length);
  };
  Vector2D.prototype.addLocal_aaofr4$ = function (vector) {
    this.x = this.x + vector.x;
    this.y = this.y + vector.y;
    return this;
  };
  Vector2D.prototype.mulLocal_14dthe$ = function (n) {
    this.x = this.x * n;
    this.y = this.y * n;
    return this;
  };
  Vector2D.prototype.invLocal = function () {
    return this.mulLocal_14dthe$(-1.0);
  };
  Vector2D.prototype.interpolateLocal_ybbuwn$ = function (vector, step) {
    this.mulLocal_14dthe$(step).addLocal_aaofr4$(vector.mul_14dthe$(1.0 - step));
  };
  Vector2D.prototype.inBound_woxjsk$ = function (topLeft, bottomRight) {
    return topLeft.x < this.x && this.x < bottomRight.x && topLeft.y < this.y && this.y < bottomRight.y;
  };
  Vector2D.prototype.toString = function () {
    return 'Vector2D{' + this.x + ', ' + this.y + '}';
  };
  Vector2D.prototype.toZero = function () {
    this.x = 0.0;
    this.y = 0.0;
  };
  Vector2D.prototype.toInt = function () {
    this.x = Math.ceil(this.x);
    this.y = Math.ceil(this.y);
    return this;
  };
  Vector2D.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Vector2D',
    interfaces: [Vector2]
  };
  function Vector2D_init(x, y, $this) {
    $this = $this || Object.create(Vector2D.prototype);
    Vector2D.call($this, x, y);
    return $this;
  }
  function Background(url, isPattern) {
    if (isPattern === void 0)
      isPattern = false;
    Decoration.call(this, 'background', url);
    this.isPattern = isPattern;
    this.sizes_0 = getScreenSize();
    if (this.isPattern)
      this.sprite.createPattern_rfwhpy$();
  }
  Background.prototype.draw_cdh3rs$ = function (dt, ctx) {
    this.sprite.draw_c0ii6e$(ctx, 0.0, 0.0, this.sprite.img.width, this.sprite.img.height, 0.0, 0.0, this.sizes_0.width, this.sizes_0.height);
  };
  Background.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Background',
    interfaces: [Decoration]
  };
  function Player(name, childNodes) {
    PhysicalNode.call(this, name, childNodes.slice());
    this.speed = 0.6;
    this.sprite_0 = new Spritesheet('assets/coin.png');
    this.numberOfFrames_0 = 10.0;
    this.steps_0 = Math.abs(1 / this.speed) | 0;
    var screenSize = getScreenSize();
    this.width = 50;
    this.height = 40;
    this.type = Physics$Types_getInstance().DYNAMIC;
    this.position = new Vector2D(screenSize.width / 2, screenSize.height / 2);
    this.leftTopCorner_0 = 0.0;
    this.currentStep_0 = 0;
  }
  function Player$handleInput$lambda(closure$dt, this$Player) {
    return function () {
      this$Player.acceleration.x = this$Player.acceleration.x - 2 * closure$dt;
    };
  }
  function Player$handleInput$lambda_0(closure$dt, this$Player) {
    return function () {
      this$Player.acceleration.x = this$Player.acceleration.x + 2 * closure$dt;
    };
  }
  function Player$handleInput$lambda_1(closure$dt, this$Player) {
    return function () {
      this$Player.acceleration.y = this$Player.acceleration.y - 4 * closure$dt;
    };
  }
  Player.prototype.handleInput_14dthe$ = function (dt) {
    Core_getInstance().inputManager.onAnalogInput_a4mwiz$('a', Player$handleInput$lambda(dt, this));
    Core_getInstance().inputManager.onAnalogInput_a4mwiz$('d', Player$handleInput$lambda_0(dt, this));
    Core_getInstance().inputManager.onAnalogInput_a4mwiz$(' ', Player$handleInput$lambda_1(dt, this));
  };
  Player.prototype.draw_cdh3rs$ = function (dt, ctx) {
    this.sprite_0.draw_c0ii6e$(ctx, 44.0 * this.leftTopCorner_0, 0.0, 44.0, 40.0, this.position.x, this.position.y, 44.0, 40.0);
    if (this.currentStep_0 === this.steps_0) {
      this.leftTopCorner_0 = this.leftTopCorner_0 + 1;
      this.currentStep_0 = 0;
    }
     else {
      this.currentStep_0 = this.currentStep_0 + 1 | 0;
    }
    if (this.leftTopCorner_0 === this.numberOfFrames_0)
      this.leftTopCorner_0 = 0.0;
  };
  Player.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Player',
    interfaces: [Interfaces$Controllable, Interfaces$Drawable, PhysicalNode]
  };
  function Scene_GamingClub() {
    var backgroundLayer = Core_getInstance().layerManager.getLayer_qbaqzy$('Background', getScreenSize());
    var mainLayer = Core_getInstance().layerManager.getLayer_qbaqzy$('Kanvasie', getScreenSize());
    backgroundLayer.attach_dkxqd5$([new Background('assets/sky.png', false)]);
    backgroundLayer.isStatic = true;
    backgroundLayer.canvas.setAttribute('sort-order', '-100');
    mainLayer.canvas.setAttribute('sort-order', '0');
    var player = new Player('Player', []);
    mainLayer.attach_dkxqd5$([player]);
    Physics_getInstance().addTo_7issw$([player]);
    Core_getInstance().camera.follow_ottzaq$(player);
    Core_getInstance().addToRunningQueue_8agu7i$([new Pair(backgroundLayer, 0.001)]);
    Core_getInstance().addToRunningQueue_8agu7i$([new Pair(mainLayer, 0.001)]);
    console.log(mainLayer.toString());
    var mapLoader = new SceneLoadManager();
    mapLoader.load_61zpoe$('assets/map/map.def').then_ugvufg$(Scene_GamingClub_init$lambda);
  }
  function Scene_GamingClub_init$lambda($receiver, response) {
    var t = new MapParser(response.responseText);
  }
  Scene_GamingClub.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Scene_GamingClub',
    interfaces: []
  };
  function Scene_TempleOfCoin() {
    var mainLayer = Core_getInstance().layerManager.getLayer_qbaqzy$('Kanvasie', getScreenSize());
    mainLayer.attach_dkxqd5$([new Player('Player', [])]);
    var player_2 = new Player('2', []);
    player_2.position.add_ljbjnb$(new Vector2D(10.0, 10.0));
    mainLayer.attach_dkxqd5$([player_2]);
  }
  Scene_TempleOfCoin.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Scene_TempleOfCoin',
    interfaces: []
  };
  function Node(name, childNodes) {
    if (name === void 0)
      name = '';
    this.name = name;
    this.position = new Vector2D(0.0, 0.0);
    this.rotation = 0.0;
    this.scale = 1.0;
    this.parent = null;
    this.children = [];
    this.dims_bnf7g8$_0 = getScreenSize();
    this.attach_dkxqd5$(childNodes.slice());
    this.i = 0;
  }
  Object.defineProperty(Node.prototype, 'dims', {
    get: function () {
      return this.dims_bnf7g8$_0;
    },
    set: function (dims) {
      this.dims_bnf7g8$_0 = dims;
    }
  });
  Node.prototype.attach_dkxqd5$ = function (nodes) {
    var tmp$;
    for (tmp$ = 0; tmp$ !== nodes.length; ++tmp$) {
      var element = nodes[tmp$];
      element.parent = this;
      this.children = this.children.concat([element]);
    }
  };
  Node.prototype.detach_ottzaq$ = function (node) {
    drop(this.children, indexOf(this.children, node));
  };
  Node.prototype.traverse_s7vmat$ = function (callback) {
    callback(this);
    var $receiver = this.children;
    var tmp$;
    for (tmp$ = 0; tmp$ !== $receiver.length; ++tmp$) {
      var element = $receiver[tmp$];
      element.traverse_s7vmat$(callback);
    }
  };
  Node.prototype.isVisible = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    this.i = this.i + 1 | 0;
    if (this.parent == null)
      return false;
    var bottomRight = ((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position.copy();
    bottomRight.invLocal();
    bottomRight.addLocal_aaofr4$(new Vector2D(this.dims.width + 64, this.dims.height + 64));
    if (this.i % 700 === 0) {
      tmp$_1 = ((tmp$_0 = this.parent) != null ? tmp$_0 : Kotlin.throwNPE()).position;
      console.log(tmp$_1, bottomRight);
    }
    if (this.position.inBound_woxjsk$(((tmp$_2 = this.parent) != null ? tmp$_2 : Kotlin.throwNPE()).position, bottomRight))
      return true;
    return false;
  };
  Node.prototype.toString = function () {
    var s = this.name;
    if (!(this.children.length === 0)) {
      var $receiver = this.children;
      var destination = Kotlin.kotlin.collections.ArrayList_init_ww73n8$($receiver.length);
      var tmp$;
      for (tmp$ = 0; tmp$ !== $receiver.length; ++tmp$) {
        var item = $receiver[tmp$];
        destination.add_11rb$('\n\t ' + item.toString());
      }
      s += ' => ' + Kotlin.toString(destination);
    }
     else {
      s += ' => <empty>\n';
    }
    return s;
  };
  Node.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Node',
    interfaces: []
  };
  function PhysicalNode(name, childNodes) {
    Node.call(this, name, childNodes.slice());
    this.canBeRotated_3effn5$_0 = false;
    this.restitution_3effn5$_0 = 0.0;
    this.density_3effn5$_0 = 0.0;
    this.friction_3effn5$_0 = 0.0;
    this.velocity_3effn5$_0 = new Vector2D(0.0, 0.0);
    this.acceleration = new Vector2D(0.0, 0.0);
    this.halfWidth_3effn5$_0 = 0;
    this.halfHeight_3effn5$_0 = 0;
    this.width = 0;
    this.height = 0;
    this.collision = Physics$Types_getInstance().DISPLACE;
    this.type = Physics$Types_getInstance().STATIC;
    this.halfWidth_3effn5$_0 = this.width * 0.5;
    this.halfHeight_3effn5$_0 = this.height * 0.5;
    this.updateBounds();
  }
  PhysicalNode.prototype.updateBounds = function () {
    this.halfWidth_3effn5$_0 = this.width * 0.5;
    this.halfHeight_3effn5$_0 = this.height * 0.5;
  };
  PhysicalNode.prototype.getMidX = function () {
    var tmp$;
    return this.halfWidth_3effn5$_0 + ((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position.x + this.position.x;
  };
  PhysicalNode.prototype.getMidY = function () {
    var tmp$;
    return this.halfHeight_3effn5$_0 + ((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position.y + this.position.y;
  };
  PhysicalNode.prototype.getTop = function () {
    var tmp$;
    return ((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position.y + this.position.y;
  };
  PhysicalNode.prototype.getLeft = function () {
    var tmp$;
    return ((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position.x + this.position.x;
  };
  PhysicalNode.prototype.getRight = function () {
    var tmp$;
    return ((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position.x + this.position.x + this.width;
  };
  PhysicalNode.prototype.getBottom = function () {
    var tmp$;
    return ((tmp$ = this.parent) != null ? tmp$ : Kotlin.throwNPE()).position.y + this.position.y + this.height;
  };
  PhysicalNode.prototype.checkCollisions_1o88it$ = function (dt, nodes) {
    var tmp$;
    tmp$ = nodes.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      action$break: {
        if (this === element)
          break action$break;
        if (Kotlin.isType(element, PhysicalNode) && this !== element) {
          if (Collision_getInstance().detectRect_2nusms$(this, element)) {
            Collision_getInstance().resolve_2nusms$(this, element);
          }
        }
      }
    }
  };
  PhysicalNode.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'PhysicalNode',
    interfaces: [Node]
  };
  _.main_kand9s$ = main;
  var package$core = _.core || (_.core = {});
  Object.defineProperty(package$core, 'Core', {
    get: Core_getInstance
  });
  var package$platformer = _.platformer || (_.platformer = {});
  package$platformer.Game = Game;
  Interfaces.Destructable = Interfaces$Destructable;
  Interfaces.Controllable = Interfaces$Controllable;
  Interfaces.Drawable = Interfaces$Drawable;
  var package$utils = _.utils || (_.utils = {});
  package$utils.Interfaces = Interfaces;
  var package$Entities = package$core.Entities || (package$core.Entities = {});
  package$Entities.Decoration = Decoration;
  package$Entities.LightSource = LightSource;
  var package$Managers = package$core.Managers || (package$core.Managers = {});
  package$Managers.CameraManager = CameraManager;
  package$Managers.InputManager = InputManager;
  package$Managers.LayerManager = LayerManager;
  package$Managers.MapParser = MapParser;
  package$Managers.SceneLoadManager = SceneLoadManager;
  package$Managers.TileMapManager = TileMapManager;
  Object.defineProperty(package$Managers, 'TileSettings', {
    get: TileSettings_getInstance
  });
  package$Managers.Tile = Tile;
  var package$Physics = package$core.Physics || (package$core.Physics = {});
  Object.defineProperty(package$Physics, 'Collision', {
    get: Collision_getInstance
  });
  Object.defineProperty(Physics.prototype, 'Types', {
    get: Physics$Types_getInstance
  });
  Object.defineProperty(package$Physics, 'Physics', {
    get: Physics_getInstance
  });
  var package$Primitives = package$core.Primitives || (package$core.Primitives = {});
  package$Primitives.Dimensions_init_vux9f0$ = Dimensions_init;
  package$Primitives.Dimensions = Dimensions;
  package$Primitives.Layer = Layer;
  package$Primitives.Pattern = Pattern;
  package$Primitives.Spritesheet = Spritesheet;
  package$Primitives.getScreenSize = getScreenSize;
  package$Primitives.Vector2 = Vector2;
  package$Primitives.Vector2D_init_vux9f0$ = Vector2D_init;
  package$Primitives.Vector2D = Vector2D;
  var package$Entities_0 = package$platformer.Entities || (package$platformer.Entities = {});
  package$Entities_0.Background = Background;
  package$Entities_0.Player = Player;
  var package$scene = package$platformer.scene || (package$platformer.scene = {});
  package$scene.Scene_GamingClub = Scene_GamingClub;
  package$scene.Scene_TempleOfCoin = Scene_TempleOfCoin;
  var package$Nodes = package$Primitives.Nodes || (package$Primitives.Nodes = {});
  package$Nodes.Node = Node;
  package$Nodes.PhysicalNode = PhysicalNode;
  Kotlin.defineModule('Kanvasie', _);
  main([]);
  return _;
}(typeof Kanvasie === 'undefined' ? {} : Kanvasie, kotlin);
